# BHAX

A BHAX csatorna forráskódjai. 
(Élő adások: https://www.twitch.tv/nbatfai, archívum: https://www.youtube.com/c/nbatfai)
A csatorna célja a szervezett számítógépes játék és a programozás népszerűsítése, tanítása, különös tekintettel a mesterséges intelligenciára! 
Hosszabb távon utánpótlás esport csapatok szervezésének tartalmi támogatása. 


P1TP
__________________________________
| előadás        |     labor     |
| -------------- | ------------- |
| +2 - SMNIST    | +2 CSIGA      |
| +2 - BRAINB    | +3 3.szakkor  | 
| +2 - 28 Pipacs | +3 4.szakkor  |
| +1 - BOGOMIPS  | +2 5x5x5      |
|  +2 22 pipacs  |               |
| +1 - E.a exor  | +5 javitott   |
| +1 - exortores | +5 javitottv2 |
|                | +10 - RF3     |
| +2 - 7x7x7     |+10 first c++  | 
| +3 04.01 ea    |+8 second cpp  |
| +3 04.08 ea    |+12 third cpp  |
| +3 04.15 ea    |+64 c++ RFHIII |
| +4 debugalloc  |+3 leet        |
| +6 bogoalloc   |+3 atszinezes  |
| +6 04.22 ea    |+40 RFH4 kvali |
| +10 tutoráltság|+128 RFH4 C++  |
| +10 tutoráltság|               |
| +6 05.06 ea    |               |
| +21 TF pelda   |               |
| +10 RF 5 virag |               |
|                |               |
|________________|_______________|
       =95              =298
