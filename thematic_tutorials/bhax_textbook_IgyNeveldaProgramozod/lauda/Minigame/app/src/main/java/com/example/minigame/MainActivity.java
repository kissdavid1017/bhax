package com.example.minigame;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    int user=0;
    int comp=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView tv = (TextView) findViewById(R.id.textView5);
        TextView tv2 = (TextView) findViewById(R.id.textView4);
        TextView tv3 = (TextView) findViewById(R.id.textView6);


    }
    public void eredmeny()
    {
        TextView tv = (TextView) findViewById(R.id.textView5);
        TextView tv2 = (TextView) findViewById(R.id.textView4);
        TextView tv3 = (TextView) findViewById(R.id.textView6);
        TextView score = (TextView) findViewById(R.id.score);

        String uservalaszt=tv.getText().toString();
        String compvalaszt=tv2.getText().toString();



        if(compvalaszt.equals(uservalaszt))
        {
            tv3.setText("Döntetlen");
        }
        else if(tv.getText().equals("Kő") && tv2.getText().equals("Olló")|| tv.getText().equals("Papir") && tv2.getText().equals("Kő")||tv.getText().equals("Olló") && tv2.getText().equals("Papir"))
        {
            user++;
            tv3.setText("Nyertel");

        }
        else if(tv2.getText().equals("Kő") && tv.getText().equals("Olló") || tv2.getText().equals("Papir") && tv.getText().equals("Kő")||tv2.getText().equals("Olló") && tv.getText().equals("Papir"))
        {
            tv3.setText("Veszitettél");
            comp++;
        }
        score.setText(user+" : "+comp);
        if(user+1<comp || comp+1<user)
        {
            tv3.setText("Vége");
            AlertDialog.Builder altdial = new AlertDialog.Builder(MainActivity.this);
            altdial.setMessage("Szeretnél új játékot kezdeni?").setCancelable(false)
                    .setPositiveButton("Igen", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            user=0;
                            comp=0;
                            tv3.setText("Eredmény");
                            score.setText("0 : 0");
                            tv.setVisibility(-1);
                            tv2.setVisibility(-1);

                        }
                    })
                    .setNegativeButton("Nem", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });

            AlertDialog alert = altdial.create();
            alert.setTitle("Dialog Header");
            alert.show();

        }
    };





           /* user=0;
            comp=0;
            tv3.setText("Eredmény");
            score.setText("0 : 0");
            tv.setVisibility(-1);
            tv2.setVisibility(-1);*/






    public void comp()
    {
        TextView tv2 = (TextView) findViewById(R.id.textView4);
        String [] szavak={"Kő","Papir","Olló"};
        Random random=new Random();
        int valaszt=random.nextInt(szavak.length);
        tv2.setText(szavak[valaszt]);
    }


   public void visibility()
   {
       TextView tv = (TextView)findViewById(R.id.textView5);
       tv.setVisibility(1);
       TextView tv2 = (TextView)findViewById(R.id.textView4);
       tv2.setVisibility(1);

   }




    public void resetButton(View view) {
        TextView tv = (TextView)findViewById(R.id.textView5);
        tv.setVisibility(-1);
        TextView tv2 = (TextView) findViewById(R.id.textView4);
        tv2.setVisibility(-1);
        TextView score = (TextView) findViewById(R.id.score);
        score.setText("0 : 0");
        user=0;
        comp=0;

    }


    public void ko(View view) {
        TextView tv = (TextView)findViewById(R.id.textView5);
        tv.setText("Kő");
        visibility();
        comp();
        eredmeny();

    }

    public void ollo(View view) {
        TextView tv = (TextView)findViewById(R.id.textView5);
        tv.setText("Olló");
        visibility();
        comp();
        eredmeny();
    }

    public void papir(View view) {
        TextView tv = (TextView)findViewById(R.id.textView5);
        tv.setText("Papir");
        visibility();
        comp();
        eredmeny();
    }


    public void kilepes(View view) {
        System.exit(0);


    }
}