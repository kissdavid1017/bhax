package Binfatest;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Test;

public class Bintest {
	LZWBinFa binfa=new LZWBinFa();
	
	@Test
	public void test()
	{
		for(char c: "01111001001001000111".toCharArray())
		{
			binfa.egyBitFeldolg(c);
		}
		assertEquals(4,binfa.getMelyseg());
		assertEquals(2.75, binfa.getAtlag());
		assertEquals(0.957427,binfa.getSzoras(),0.0001);
		
	}

}
