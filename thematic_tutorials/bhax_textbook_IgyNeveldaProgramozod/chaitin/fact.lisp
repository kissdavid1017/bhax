#!/usr/bin/clisp

(defun factorialis_rekurziv(n)     ;definiáljuk a függvényünket ami az n paramétert kapja,mert erre hívjuk meg majd(a számra)
	(if(= n 0) ;eloszor a speciális eseteket kezeljuk,ha n=0 akkor az érték adott tehát 1
		1
		(* n (factorialis_rekurziv(- n 1)))) )

(format t "Recursive:~%")

 (loop for i from 0 to 20
 	do (format t "~D! = ~D~%" i(factorialis_rekurziv i)))    ;a formattal kiiratjuk
 ; eloszor kiirjuk az i!-t ~D! ez ide helyettesítődik be a következő helyre behelyettesítődik az i re meghívódó faktoriális függvény

 (defun factorialis_iterativ(n)
 	(let((f 1));létrehozunk egy f változót 1 értékkel 
 		(dotimes (i n)  ;az i re n-szer
 		(setf f(* f (+ i 1))))     ;az f értékét folyton felülírjuk tehát i +1  lesz mindig
 		f; visszaadjuk az f értékét
 	)
 )

 (format t "iterativ:~%")



(loop for i from 0 to 20
 	do (format t "~D! = ~D~%" i(factorialis_iterativ i)))