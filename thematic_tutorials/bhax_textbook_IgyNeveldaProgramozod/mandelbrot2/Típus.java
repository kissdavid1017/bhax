
import java.util.*;


/**
 * Class Irodalmi
 */
public class Irodalmi {

  //
  // Fields
  //

  
  //
  // Constructors
  //
  public Irodalmi () { };
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

  //
  // Other methods
  //

}


import java.util.*;


/**
 * Class Típus
 */
public class Típus {

  //
  // Fields
  //

  private String Típusnév;
  
  //
  // Constructors
  //
  public Típus () { };
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

  /**
   * Set the value of Típusnév
   * @param newVar the new value of Típusnév
   */
  private void setTípusnév (String newVar) {
    Típusnév = newVar;
  }

  /**
   * Get the value of Típusnév
   * @return the value of Típusnév
   */
  private String getTípusnév () {
    return Típusnév;
  }

  //
  // Other methods
  //

}
